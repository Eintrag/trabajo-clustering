function [ ] = identificarelementosenlamatriz( M, cidx, K, nTipos, nRegiones )
%IDENTIFICARELEMENTOSENLAMATRIZ
%   Identifica el tipo y grupo de los elementos agrupados
for grupo=1:K
    fprintf('\nElementos en el Grupo %d: %d \n', ...
        [grupo sum(cidx(:) == grupo)])
    nElementosPorTipoYRegion = zeros(nRegiones, nRegiones);
    for j=1:size(cidx, 1)
        for tipo=1:nTipos
            for region=1:nRegiones
            if (M(j, 1) == tipo && M(j, 2) == region && cidx(j) == grupo)
                nElementosPorTipoYRegion(tipo, region) = ...
                    nElementosPorTipoYRegion(tipo, region) + 1;
            end
            end
        end
    end
    for tipo=1:nTipos
        fprintf('\tTipo %d: %d\n', ...
            [ tipo sum(nElementosPorTipoYRegion(tipo, :)) ])
        for region=1:nRegiones
             fprintf('\t\tRegi�n %d: %d\n', ...
            [ region nElementosPorTipoYRegion(tipo, region) ])
        end
    end
end
end