function [ ] = plotpca2d( Mpca )
%PLOTPCA2D
hold on
plot(Mpca(:,1), Mpca(:,2), 'x', 'MarkerSize', 4)
    for i=1:size(Mpca, 1)
        text(Mpca(i, 1), Mpca(i, 2), num2str(i));
    end
axis('square')
title('Datos estandarizados PCA', 'fontsize', 18)
end