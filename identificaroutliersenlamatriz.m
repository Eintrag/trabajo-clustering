function [  ] = identificaroutliersenlamatriz( indicesDeFilasOutliers, ...
    MatrizOriginal )
%IDENTIFICAROUTLIERSENLAMATRIZ 
nOutliersSegunTipoyRegion = zeros(3, 2);
outliersEnLaMatrizOriginal = [];
for i=1:size(indicesDeFilasOutliers, 2)
    outliersEnLaMatrizOriginal = [outliersEnLaMatrizOriginal; ...
        horzcat(indicesDeFilasOutliers(i),...
        MatrizOriginal(indicesDeFilasOutliers(i), :))];
    tipo = MatrizOriginal(indicesDeFilasOutliers(i), 2);
    region = MatrizOriginal(indicesDeFilasOutliers(i), 1);
    nOutliersSegunTipoyRegion(tipo, region) = ...
        nOutliersSegunTipoyRegion(tipo, region) + 1;
end
fig = figure('Name', 'Outliers en la matriz original');
uitable(fig, 'Data', outliersEnLaMatrizOriginal, 'ColumnName', ...
    {'Fila' 'Tipo' 'Regi�n' 'Frescos' 'L�cteos' 'Ultramarinos' ...
    'Congelados' 'Detergentes y papeler�a' 'Delicatessen'});
fig = figure('Name', 'N�mero de outliers seg�n tipo y regi�n');
uitable(fig, 'Data', nOutliersSegunTipoyRegion, 'ColumnName', ...
    {'Tipo 1' 'Tipo 2'}, 'RowName', {'Region 1' 'Region 2' 'Region 3'});
end



