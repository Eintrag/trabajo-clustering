%% Limpiar e inicializar variables del problema
clear, clc, close all;
MatrizOriginal = inicializarmatrizdedatos();
K = 4;
distanciaKMeans = 'sqeuclidean';
vecesKMeans = 30;
sigmaUmbralOutliers = 3;
nTipos = 2;
nRegiones = 3;
MatrizSinDosPrimerasColumnas = MatrizOriginal(:, 3:size(MatrizOriginal, 2));
DatosStd = zscore(MatrizSinDosPrimerasColumnas, 0, 1);
clear MatrizSinDosPrimerasColumnas;

%% Quitar outliers
[indicesFilasOutliers, SSE] = deteccionoutlierskmeans(DatosStd, K,...
    sigmaUmbralOutliers, distanciaKMeans, vecesKMeans);
fprintf('Numero de outliers quitados: %d\n', size(indicesFilasOutliers, 2))
indicesFilasOutliers
DatosStdNoOutliers = quitarfilasdelamatriz(DatosStd, indicesFilasOutliers);

%% Resultados para K = 4 con outliers
figure('Name', 'Resultados sin quitar outliers para K = 4');
fprintf('\nResultados sin quitar outliers para K = 4\n\n')
[cidx, centroidesConOutliers] = calcularkmeansydibujarpca(DatosStd, K, ...
    vecesKMeans, distanciaKMeans);
identificarelementosenlamatriz(MatrizOriginal, cidx , K, nTipos, nRegiones);

%% Resultados para K = 4 sin outliers
figure('Name', 'Resultados quitando outliers para K = 4');
fprintf('\nResultados quitando outliers para K = 4\n\n')
[cidx, centroidesSinOutliers] = calcularkmeansydibujarpca(...
    DatosStdNoOutliers, K, vecesKMeans, distanciaKMeans);
centroidesSinOutliers
identificarelementosenlamatriz(quitarfilasdelamatriz(MatrizOriginal, ...
    indicesFilasOutliers), cidx, K, nTipos, nRegiones);

%% Aplicar BIC para cambiar el n�mero de grupos
figure('Name', 'Valores de K aplicando BIC');
K = aplicarbicparahallarngrupos(DatosStd, vecesKMeans, 12);

%% Resultados para mejor K seg�n BIC
%% Quitar outliers
[indicesFilasOutliers, SSE] = deteccionoutlierskmeans(DatosStd, K,...
    sigmaUmbralOutliers, distanciaKMeans, vecesKMeans);
fprintf('Numero de outliers quitados: %d\n', size(indicesFilasOutliers, 2))
indicesFilasOutliers
DatosStdNoOutliers = quitarfilasdelamatriz(DatosStd, indicesFilasOutliers);

figure('Name', 'Resultados quitando outliers para mejor K seg�n BIC');
fprintf('Resultados quitando outliers para mejor K seg�n BIC\n\n')
[cidx, centroidesSinOutliers] = calcularkmeansydibujarpca(...
    DatosStdNoOutliers, K, vecesKMeans, distanciaKMeans);
centroidesSinOutliers
identificarelementosenlamatriz(quitarfilasdelamatriz(MatrizOriginal, ...
    indicesFilasOutliers), cidx, K, nTipos, nRegiones);

identificaroutliersenlamatriz(indicesFilasOutliers, MatrizOriginal);
