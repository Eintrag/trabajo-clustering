function [ ] = plotpca2dcoloreadosporcluster( Mpca, nGrupos, cidx, ...
    centroidesPca )
Colores = ['b', 'r', 'g', 'k', 'y', 'c', 'm']; 
% Si es necesario, podemos combinar colores y formas
if size(Colores) < nGrupos
    fprintf(2,['El n�mero de clusteres es mayor que el de colores definidos.\n' ...
        'Considera editar el c�digo para combinar colores y formas.\n']);
end
leyenda = cell(nGrupos, 1); 
axis('square')
title('Datos estandarizados PCA por cl�ster', 'fontsize', 18)
for i=1:nGrupos
    hold on
    plot(Mpca(cidx==i,1), Mpca(cidx==i,2), 'x', 'MarkerSize', 4, ...
        'MarkerEdgeColor', Colores(i), 'MarkerFaceColor', Colores(i));
    leyenda{i} = ['Grupo ' num2str(i)];
end
for i=1:size(centroidesPca, 1)
    hold on
    plot(centroidesPca(i, 1), centroidesPca(i, 2), 'o', 'MarkerSize', 10, ...
        'MarkerEdgeColor', 'k', 'MarkerFaceColor', Colores(i));
end
legend(leyenda)
end