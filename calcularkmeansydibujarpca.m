function [ cidx, centroides, sumd ] = calcularkmeansydibujarpca( M, K, ...
    nVecesCalcularKMeans, distanciaParaKmeans)
%CALCULARKMEANSYDIBUJARPCA 
[cidx, centroides, sumd, D] = kmeans(M, K, ...
    'Replicates', nVecesCalcularKMeans, 'Distance', distanciaParaKmeans);
[~, Mpca] = pca(M, 'NumComponents', 2);
[~, centroidesPca] = pca(centroides, 'NumComponents', 2);
subplot(1, 2, 1), plotpca2d(Mpca)
subplot(1, 2, 2), plotpca2dcoloreadosporcluster(Mpca, K, cidx, centroidesPca)
end