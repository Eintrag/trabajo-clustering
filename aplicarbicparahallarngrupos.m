function [ mejorKSegunBIC ] = aplicarbicparahallarngrupos( ...
    MatrizDeDatosEstandarizados, nVecesCalcularKMeans, KMax )
%APLICARBICPARAHALLARNGRUPOS
% Decide el mejor valor para el numero de grupos seg�n el valor de la
% funci�n BIC y dibuja los valores de BIC(numeroDeGrupos)
for K=2:KMax
    [cidx] = kmeans(MatrizDeDatosEstandarizados, K, 'Replicates', ...
        nVecesCalcularKMeans);
    [Bic_K, ~]=BIC(K, cidx, MatrizDeDatosEstandarizados);
    BICK(K)=Bic_K;
end
BICK(1) = Inf(1);

plot(2:KMax, BICK(2:KMax)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
set(gca, 'xtick', [2:KMax]);
xlabel('K','fontsize',18)
ylabel('BIC(K)','fontsize',18)

[~, mejorKSegunBIC] = min(BICK(:));

end