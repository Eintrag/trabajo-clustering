function [ MsinAlgunasFilas ] = quitarfilasdelamatriz( M,...
    indicesDeFilasAQuitar )
%QUITARFILASDELAMATRIZ 
MsinAlgunasFilas = zeros(size(M, 1) - size(indicesDeFilasAQuitar, 2), ...
    size(M, 2));
cursorDeMSinAlgunasFilas = 1;
for i=1:size(M, 1)
    if ~any(indicesDeFilasAQuitar == i)
        MsinAlgunasFilas(cursorDeMSinAlgunasFilas, 1:size(M, 2)) = ...
            M(i, 1:size(M, 2));
        cursorDeMSinAlgunasFilas = cursorDeMSinAlgunasFilas + 1;  
    end
end
end