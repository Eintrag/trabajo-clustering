function [ indicesDeFilasOutliers, SSE ] = ...
    deteccionoutlierskmeans( M, nGrupos, nDesviacionesTipicasUmbral,...
    distanciaParaKmeans, nVecesCalcularKMeans )
%DETECCIONOUTLIERSKMEANS Jackknife aplicado a kmeans para detectar outliers
% Utiliza c�digo proporcionado por Ricardo Garc�a R�denas en la asignatura
% Desarrollo de Sistemas Inteligentes

SSE = zeros(size(M, 1), 1);
for i=1:size(M, 1)
    M_sin_i=[M(1:(i-1),:);M((i+1):size(M, 1),:)]; % eliminar la observaci�n i
    [~, ~, sumd] = kmeans(M_sin_i, nGrupos, 'Replicates', ... 
        nVecesCalcularKMeans, 'Distance', distanciaParaKmeans);
    SSE(i)=sum(sumd); % sumatorio de distancias intraclusters al cuadrado 
    % para obtener SSE                      
end

desviacionTipica = std(SSE);    
media = mean(SSE);
indicesDeFilasOutliers = [];
for i=1:size(M, 1)
    if abs(SSE(i) - media) > nDesviacionesTipicasUmbral * desviacionTipica
        indicesDeFilasOutliers = [indicesDeFilasOutliers, i];
    end
end
end